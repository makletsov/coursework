val ZIOCatsVersion      = "2.2.0.1"
val LogbackVersion      = "1.2.3"
val CirceVersion        = "0.14.0-M1"
val CirceOpticsVersion  = "0.13.0"
val ZIOVersion          = "1.0.3"
val CatsVersion         = "2.2.0"
val CatsEffectVersion   = "2.0.0"
val HtmlUnitVersion     = "2.45.0"
val MonixVersion        = "3.3.0"
val Http4sVersion       = "0.21.8"
val ScalaUriVersion     = "3.0.0"
val DoobieVersion       = "0.9.4"
val ScalaTestVersion    = "3.2.0"
val FlyWayVersion       = "7.3.0"
val PureConfigVersion   = "0.14.0"
val ZIOLoggingVersion   = "0.5.4"
val Log4jVersion        = "2.14.0"

lazy val root = (project in file("."))
  .enablePlugins(JavaAppPackaging)
  .settings(
    organization := "makletsov",
    name := "web-movie-crawler",
    version := "0.0.1",
    scalaVersion := "2.13.4",
    libraryDependencies ++= Seq(
      "org.typelevel"              %% "cats-core"            % CatsVersion,
      "org.typelevel"              %% "cats-effect"          % CatsVersion,
      "org.http4s"                 %% "http4s-blaze-server"  % Http4sVersion,
      "org.http4s"                 %% "http4s-dsl"           % Http4sVersion,
      "org.tpolecat"               % "doobie-core_2.13"      % DoobieVersion,
      "org.tpolecat"               % "doobie-postgres_2.13"  % DoobieVersion,
      "org.tpolecat"               % "doobie-h2_2.13"        % DoobieVersion,
      "org.tpolecat"               % "doobie-hikari_2.13"    % DoobieVersion,
      "io.circe"                   %% "circe-core"           % CirceVersion,
      "io.circe"                   %% "circe-generic"        % CirceVersion,
      "io.circe"                   %% "circe-parser"         % CirceVersion,
      "io.circe"                   %% "circe-literal"        % CirceVersion,
      "io.circe"                   %% "circe-optics"         % CirceOpticsVersion,
      "org.flywaydb"               % "flyway-core"           % FlyWayVersion,
      "org.apache.logging.log4j"   % "log4j-api"             % Log4jVersion,
      "org.apache.logging.log4j"   % "log4j-core"            % Log4jVersion,
      "org.apache.logging.log4j"   % "log4j-slf4j-impl"      % Log4jVersion,
      "com.github.pureconfig"      % "pureconfig_2.13"       % PureConfigVersion,
      "dev.zio"                    %% "zio"                  % ZIOVersion,
      "dev.zio"                    %% "zio-streams"          % ZIOVersion,
      "dev.zio"                    %% "zio-logging"          % ZIOLoggingVersion,
      "dev.zio"                    %% "zio-logging-slf4j"    % ZIOLoggingVersion,
      "dev.zio"                    %  "zio-interop-cats_2.13" % ZIOCatsVersion,
      "io.monix"                   %% "monix"                % MonixVersion,
      "org.http4s"                 %% "http4s-dsl"           % Http4sVersion,
      "org.http4s"                 %% "http4s-circe"         % Http4sVersion,
      "org.http4s"                 %% "http4s-blaze-server"  % Http4sVersion,
      "org.http4s"                 %% "http4s-blaze-client"  % Http4sVersion,
      "net.sourceforge.htmlunit"   % "htmlunit"              % HtmlUnitVersion,
      "io.lemonlabs"               %% "scala-uri"            % ScalaUriVersion,
      "org.scalatest"              %% "scalatest"            % ScalaTestVersion % Test,
      "org.tpolecat"               %% "doobie-scalatest"     % DoobieVersion    % Test,
      "dev.zio"                    %% "zio-test-sbt"         % ZIOVersion       % Test,
      "dev.zio"                    %% "zio-test"             % ZIOVersion       % Test,
      "dev.zio"                    %% "zio-test-magnolia"    % ZIOVersion       % Test,
      "com.opentable.components"   % "otj-pg-embedded"       % "0.13.3"         % Test,
    ),
    addCompilerPlugin("org.spire-math" % "kind-projector_2.13.0-RC1" % "0.9.10"),
    addCompilerPlugin("com.olegpy"     % "better-monadic-for_2.13"   % "0.3.1")
  )

testFrameworks += new TestFramework("zio.test.sbt.ZTestFramework")

scalacOptions ++= Seq(
  "-deprecation",               // Emit warning and location for usages of deprecated APIs.
  "-encoding", "UTF-8",         // Specify character encoding used by source files.
  "-language:higherKinds",      // Allow higher-kinded types
  "-language:postfixOps",       // Allows operator syntax in postfix position (deprecated since Scala 2.10)
  "-feature",                   // Emit warning and location for usages of features that should be imported explicitly.
  //"-Ypartial-unification",      // Enable partial unification in type constructor inference
  "-Xfatal-warnings",           // Fail the compilation if there are any warnings
)