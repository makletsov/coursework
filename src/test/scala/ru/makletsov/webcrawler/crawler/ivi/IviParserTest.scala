package ru.makletsov.webcrawler.crawler.ivi

import zio.test._
import zio.logging.Logging
import zio.test.Assertion._
import io.circe.literal._
import io.lemonlabs.uri.Url
import ru.makletsov.webcrawler.crawler.ivi.parser._

object IviParserTest extends DefaultRunnableSpec {
  val url: Url = Url(scheme = "https", host = "www.test.com")
  val parser   = new IviParserImpl
  val invalidJson = json"""{ "result": [] }"""

  def spec = suite("IviParserSpec")(
    testM("parseCategories returns categories collection from valid json") {
      val json = json"""
            {
              "result": [
                {
                  "id": 23,
                  "title": "Спорт",
                  "hru": "sport",
                  "priority": 11,
                  "description": "",
                  "content_count": 228,
                  "compilation_count": 25,
                  "catalogue_count": 253,
                  "genres": [
                    {
                      "id": 278,
                      "category_id": 23,
                      "title": "Единоборства",
                      "priority": 14,
                      "hru": "edinoborstva",
                      "background": [],
                      "content_count": 7,
                      "compilation_count": 5,
                      "catalogue_count": 12
                    },
                    {
                      "id": 275,
                      "category_id": 23,
                      "title": "Фитнес",
                      "priority": 13,
                      "hru": "fitness",
                      "background": [],
                      "content_count": 221,
                      "compilation_count": 19,
                      "catalogue_count": 240
                    }
                  ],
                  "meta_genres": []
                }
              ]
            }"""

      val requiredValue = Seq(IviCategory(23, "Спорт", Vector(IviGenre(278, 23, "Единоборства", 12), IviGenre(275, 23, "Фитнес", 240))))

      parser
        .parseCategories(url, json)
        .provideLayer(Logging.ignore)
        .map(
          assert(_)(equalTo(requiredValue))
        )
    },
    testM("parseCategories returns empty collection from invalid json") {
      parser
        .parseCategories(url, invalidJson)
        .provideLayer(Logging.ignore)
        .map(
          assert(_)(equalTo(Seq.empty[IviCategory]))
        )
    },
    testM("parseContentItems returns compilations from valid json") {
      val json = json"""
            {
              "result": [
                {
                  "content_paid_types" : [
                    "AVOD"
                  ],
                  "id" : 12234,
                  "object_type" : "compilation",
                  "orig_title" : "Det som göms i snö",
                  "title" : "Что спрятано в снегу",
                  "years" : [
                    2018
                  ],
                  "seasons_count" : 1
                }
              ]
            }"""

      val requiredValue = Seq(IviCompilation(12234, "Что спрятано в снегу", "Det som göms i snö", Vector(2018), 1, Seq("AVOD")))

      parser
        .parseContentItems(url, json)
        .provideLayer(Logging.ignore)
        .map(
          assert(_)(equalTo(requiredValue))
        )
    },
    testM("parseContentItems returns empty compilations collection from invalid json") {
      parser
        .parseContentItems(url, invalidJson)
        .provideLayer(Logging.ignore)
        .map(
          assert(_)(equalTo(Seq.empty[IviContent]))
        )
    },
    testM("parseContentItems returns videos from valid json") {
      val json = json"""
          {
            "result": [
              { "content_paid_types" : [
                  "AVOD",
                  "SVOD"
                ],
                "id" : 445634,
                "object_type" : "video",
                "orig_title" : "",
                "title" : "Единоборства для детей (3-6 лет) Школа героев (3-6 лет) Выпуск 1",
                "year" : 2020
              }
            ]
          }"""

      val requiredValue = Seq(IviVideo(445634, "Единоборства для детей (3-6 лет) Школа героев (3-6 лет) Выпуск 1", "", 2020, Seq("AVOD", "SVOD")))

      parser
        .parseContentItems(url, json)
        .provideLayer(Logging.ignore)
        .map(
          assert(_)(equalTo(requiredValue))
        )
    },
    testM("parseContentItems returns empty videos collection from invalid json") {
      parser
        .parseContentItems(url, invalidJson)
        .provideLayer(Logging.ignore)
        .map(
          assert(_)(equalTo(Seq.empty[IviContent]))
        )
    },
    testM("parseSubscriptions returns subscriptions collection from valid json") {
      val json = json"""
              {
                "result": {
                  "purchase_options": [
                    {
                      "object_id": 6,
                      "object_type": "subscription",
                      "object_title": "ivi",
                      "ownership_type": "temporal",
                      "bonus_info": "",
                      "price": "0",
                      "currency": "RUB",
                      "currency_symbol": "₽",
                      "price_ranges": {
                        "price": {
                          "min": "0",
                          "max": "0"
                        },
                        "user_price": {
                          "min": "0",
                          "max": "0"
                        },
                        "discount_on_cheapest_price": null
                      },
                      "product_identifier": "ru.ivi.svod.trial.1month",
                      "product_title": "Подписка ivi 1 месяц с триалом (РФ_Android)",
                      "duration": 2592000,
                      "quality": null,
                      "preorder": false,
                      "renewal_price": "399",
                      "renewal_initial_period": 1209600,
                      "trial": true,
                      "trial_validation_price": "1",
                      "change_card": false,
                      "long_subscription": false,
                      "downloadable": null,
                      "affiliate_subscription": false,
                      "bundle_subscription": false,
                      "upsale_from_purchases": [],
                      "mobile_offer_code": null,
                      "option_hash": "ke78mP70gcDJBxBeeVyidQ"
                    }
                  ]
                }
              }"""

      val requireValue = Seq(IviSubscription("Подписка ivi 1 месяц с триалом (РФ_Android)", 0, 1209600, 399, 2592000, "RUB"))

      parser
        .parseSubscriptions(url, json)
        .provideLayer(Logging.ignore)
        .map(
          assert(_)(equalTo(requireValue))
        )
    },
    testM("parseSubscriptions returns empty subscriptions collection from invalid json") {
      parser
        .parseSubscriptions(url, invalidJson)
        .provideLayer(Logging.ignore)
        .map(
          assert(_)(equalTo(Seq.empty[IviSubscription]))
        )
    },
    testM("parsePurchases returns purchases collection from valid json") {
      val json =
        json"""
          {
            "result": {
              "purchase_options": [
                {
                  "object_id": 6268,
                  "object_type": "season",
                  "object_title": "",
                  "object_child_ids": [],
                  "ownership_type": "eternal",
                  "bonus_info": "",
                  "price": "399",
                  "currency": "RUB",
                  "currency_symbol": "₽",
                  "price_ranges": {
                    "price": {
                      "min": "399",
                      "max": "399"
                    },
                    "user_price": {
                      "min": "399",
                      "max": "399"
                    },
                    "discount_on_cheapest_price": null
                  },
                  "product_identifier": "ru.ivi.client.season.est.hd.399",
                  "product_title": "(ivi) Покупка сезона HD — 399 руб.",
                  "duration": null,
                  "quality": "HD",
                  "preorder": false,
                  "renewal_price": null,
                  "renewal_initial_period": null,
                  "trial": false,
                  "trial_validation_price": null,
                  "change_card": false,
                  "long_subscription": false,
                  "downloadable": true,
                  "affiliate_subscription": false,
                  "bundle_subscription": false,
                  "upsale_from_purchases": [],
                  "option_hash": "6kL3r_itmWaCu-aqbxnQVA"
                }
              ]
            }
          }
            """

      parser
        .parsePurchases(url, json)
        .provideLayer(Logging.ignore)
        .map(
          assert(_)(equalTo(Seq.empty[IviPurchase]))
        )
    },
    testM("parsePurchases returns empty purchases collection from invalid json") {
      parser
        .parsePurchases(url, invalidJson)
        .provideLayer(Logging.ignore)
        .map(
          assert(_)(equalTo(Seq.empty[IviPurchase]))
        )
    }
  )
}
