CREATE TABLE IF NOT EXISTS video (
    id         UUID NOT NULL,
    title      VARCHAR NOT NULL,
    orig_title VARCHAR NOT NULL,
    year       INT NOT NULL
);

CREATE TABLE IF NOT EXISTS compilation (
    id            UUID NOT NULL,
    title         VARCHAR NOT NULL,
    orig_title    VARCHAR NOT NULL,
    years         INT4[] NOT NULL,
    seasons_count INT NOT NULL
);

CREATE TABLE IF NOT EXISTS subscription (
    id             UUID NOT NULL,
    vendor         VARCHAR NOT NULL,
    title          VARCHAR NOT NULL,
    trial_price    NUMERIC (7, 2),
    trial_duration INT NOT NULL,
    price          NUMERIC (7, 2),
    duration       INT NOT NULL,
    currency       VARCHAR NOT NULL,
    created        TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE IF NOT EXISTS item_subscription (
    id              UUID NOT NULL,
    item_id         UUID NOT NULL,
    subscription_id UUID NOT NULL,
    vendor          VARCHAR NOT NULL,
    created         TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE IF NOT EXISTS purchase (
    id            UUID NOT NULL,
    item_id       INT NOT NULL,
    vendor        VARCHAR NOT NULL,
    min_price     NUMERIC (7, 2),
    max_price     NUMERIC (7, 2),
    duration      INT NOT NULL,
    currency      VARCHAR NOT NULL,
    quality       VARCHAR NOT NULL,
    created       TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE IF NOT EXISTS item_purchase (
    id            UUID NOT NULL,
    item_id       UUID NOT NULL,
    foreign_id    INT NOT NULL,
    vendor        VARCHAR NOT NULL,
    created       TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE IF NOT EXISTS item_free (
    id            UUID NOT NULL,
    item_id       UUID NOT NULL,
    vendor        VARCHAR NOT NULL,
    created       TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP
);