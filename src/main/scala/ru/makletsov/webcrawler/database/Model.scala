package ru.makletsov.webcrawler.database

import java.sql.Timestamp
import java.util.UUID

sealed trait DomainObject

sealed trait Content {
  val id: UUID
}

final case class Video(
    id: UUID,
    title: String,
    origTitle: String,
    year: Int
) extends Content

final case class Compilation(
    id: UUID,
    title: String,
    origTitle: String,
    years: Vector[Int],
    seasons: Int
) extends Content

final case class Subscription(
    id: UUID,
    vendor: String,
    title: String,
    trialPrice: Double,
    trialDuration: Int,
    price: Double,
    duration: Int,
    currency: String,
    created: Timestamp
) extends DomainObject

final case class Purchase(
    id: UUID,
    foreignId: Int,
    vendor: String,
    minPrice: Double,
    maxPrice: Double,
    duration: Int,
    currency: String,
    quality: String,
    created: Timestamp
) extends DomainObject

sealed trait AccessOption extends DomainObject

final case class ItemSubscription(
    id: UUID,
    itemId: UUID,
    subId: UUID,
    vendor: String,
    created: Timestamp
) extends AccessOption

final case class ItemFreeAccess(
    id: UUID,
    itemId: UUID,
    vendor: String,
    created: Timestamp
) extends AccessOption

final case class ItemPurchase(
    id: UUID,
    itemId: UUID,
    foreignId: Int,
    vendor: String,
    created: Timestamp
) extends AccessOption

final case class VideoNotFound(video: Video) extends Exception

final case class CompilationNotFound(compilation: Compilation) extends Exception

final case class SubscriptionNotFound(subscription: Subscription) extends Exception

final case class UnknownAccessType(accessType: String) extends Exception
