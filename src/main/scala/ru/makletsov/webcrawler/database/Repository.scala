package ru.makletsov.webcrawler.database

import zio._

import java.sql.Timestamp

object Repository {

  trait Service {

    def getVideos(s: String): Task[List[Video]]

    def getCompilations(s: String): Task[List[Compilation]]

    def hasFreeAccess(c: Content): Task[List[String]]

    def getSubscriptions(c: Content): Task[List[Subscription]]

    def getPurchases(c: Content): Task[List[Purchase]]

    def createVideo(v: Video): Task[Video]

    def createCompilation(c: Compilation): Task[Compilation]

    def createFreeAccess(f: ItemFreeAccess): Task[ItemFreeAccess]

    def createSubscriptionAccess(is: ItemSubscription): Task[ItemSubscription]

    def createPurchaseAccess(ip: ItemPurchase): Task[ItemPurchase]

    def createSubscription(s: Subscription): Task[Subscription]

    def createPurchase(p: Purchase): Task[Purchase]

    def deleteSubscriptions(t: Timestamp, vendor: String): Task[Int]

    def deletePurchases(t: Timestamp, vendor: String): Task[Int]

    def deleteFreeAccess(t: Timestamp, vendor: String): Task[Int]

  }

  def getVideos(s: String): RIO[DBService, List[Video]] = RIO.accessM(_.get.getVideos(s))

  def getCompilations(s: String): RIO[DBService, List[Compilation]] =
    RIO.accessM(_.get.getCompilations(s))

  def hasFreeAccess(c: Content): RIO[DBService, List[String]] = RIO.accessM(_.get.hasFreeAccess(c))

  def getSubscriptions(c: Content): RIO[DBService, List[Subscription]] =
    RIO.accessM(_.get.getSubscriptions(c))

  def getPurchases(c: Content): RIO[DBService, List[Purchase]] = RIO.accessM(_.get.getPurchases(c))

  def createVideo(a: Video): RIO[DBService, Video] = RIO.accessM(_.get.createVideo(a))

  def createCompilation(c: Compilation): RIO[DBService, Compilation] =
    RIO.accessM(_.get.createCompilation(c))

  def createSubscription(s: Subscription): RIO[DBService, Subscription] =
    RIO.accessM(_.get.createSubscription(s))

  def createSubscriptionAccess(is: ItemSubscription): RIO[DBService, ItemSubscription] =
    RIO.accessM(_.get.createSubscriptionAccess(is))

  def createPurchase(p: Purchase): RIO[DBService, Purchase] = RIO.accessM(_.get.createPurchase(p))

  def createFreeAccess(f: ItemFreeAccess): RIO[DBService, ItemFreeAccess] =
    RIO.accessM(_.get.createFreeAccess(f))

  def createPurchaseAccess(ip: ItemPurchase): RIO[DBService, ItemPurchase] =
    RIO.accessM(_.get.createPurchaseAccess(ip))

  def deleteSubscriptions(t: Timestamp, vendor: String): RIO[DBService, Int] =
    RIO.accessM(_.get.deleteSubscriptions(t, vendor))

  def deletePurchases(t: Timestamp, vendor: String): RIO[DBService, Int] =
    RIO.accessM(_.get.deletePurchases(t, vendor))

  def deleteFreeAccess(t: Timestamp, vendor: String): RIO[DBService, Int] =
    RIO.accessM(_.get.deleteFreeAccess(t, vendor))
}