package ru.makletsov.webcrawler

import zio._

import doobie.Transactor

package object database {

  type DBTransactor = Has[Transactor[Task]]
  type DBService = Has[Repository.Service]

}
