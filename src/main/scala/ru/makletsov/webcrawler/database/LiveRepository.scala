package ru.makletsov.webcrawler.database

import java.sql.Timestamp

import zio._
import zio.interop.catz._
import zio.blocking.Blocking
import cats.effect.Blocker
import org.flywaydb.core.Flyway
import doobie._
import doobie.implicits._
import doobie.hikari._
import doobie.implicits.javatime._
import ru.makletsov.webcrawler.config._
import zio.logging._

final class LiveRepository(tnx: Transactor[Task]) extends Repository.Service {

  import LiveRepository._

  override def createVideo(v: Video): Task[Video] =
    (for {
      _   <- SQL.createVideo(v).run
      get <- SQL.getVideo(v).option
    } yield get)
      .transact(tnx)
      .foldM(
        Task.fail(_),
        o => Task.require(VideoNotFound(v))(Task.succeed(o))
      )

  override def createCompilation(c: Compilation): Task[Compilation] =
    (for {
      _   <- SQL.createCompilation(c).run
      get <- SQL.getCompilation(c).option
    } yield get)
      .transact(tnx)
      .foldM(
        Task.fail(_),
        o => Task.require(CompilationNotFound(c))(Task.succeed(o))
      )

  override def createSubscription(s: Subscription): Task[Subscription] =
    (for {
      _   <- SQL.createSubscription(s).run
      get <- SQL.getSubscription(s).option
    } yield get)
      .transact(tnx)
      .foldM(
        Task.fail(_),
        o => Task.require(SubscriptionNotFound(s))(Task.succeed(o))
      )

  override def createSubscriptionAccess(is: ItemSubscription): Task[ItemSubscription] =
    SQL
      .createItemSubscription(is)
      .run
      .transact(tnx)
      .foldM(
        Task.fail(_),
        _ => Task.succeed(is)
      )

  override def createPurchase(p: Purchase): Task[Purchase] =
    SQL
      .createPurchase(p)
      .run
      .transact(tnx)
      .foldM(
        Task.fail(_),
        _ => Task.succeed(p)
      )

  override def createFreeAccess(f: ItemFreeAccess): Task[ItemFreeAccess] =
    SQL
      .createFreeAccess(f)
      .run
      .transact(tnx)
      .foldM(
        Task.fail(_),
        _ => Task.succeed(f)
      )

  override def createPurchaseAccess(ip: ItemPurchase): Task[ItemPurchase] =
    SQL
      .createPurchaseAccess(ip)
      .run
      .transact(tnx)
      .foldM(
        Task.fail(_),
        _ => Task.succeed(ip)
      )

  override def deleteSubscriptions(t: Timestamp, vendor: String): Task[Int] =
    SQL
      .deleteSubscriptions(t, vendor)
      .run
      .transact(tnx)

  override def deletePurchases(t: Timestamp, vendor: String): Task[Int] =
    SQL
      .deletePurchases(t, vendor)
      .run
      .transact(tnx)

  override def deleteFreeAccess(t: Timestamp, vendor: String): Task[Int] =
    SQL
      .deleteFreeAccess(t, vendor)
      .run
      .transact(tnx)

  override def getVideos(s: String): Task[List[Video]] =
    SQL
      .getVideos(s)
      .to[List]
      .transact(tnx)

  override def getCompilations(s: String): Task[List[Compilation]] =
    SQL
      .getCompilations(s)
      .to[List]
      .transact(tnx)

  override def hasFreeAccess(c: Content): Task[List[String]] =
    SQL
      .hasFreeAccess(c)
      .to[List]
      .transact(tnx)

  override def getSubscriptions(c: Content): Task[List[Subscription]] =
    SQL
      .getSubscriptions(c)
      .to[List]
      .transact(tnx)

  override def getPurchases(c: Content): Task[List[Purchase]] =
    SQL
      .getPurchases(c)
      .to[List]
      .transact(tnx)
}

object LiveRepository {

  object SQL {

    import doobie.implicits.javasql.TimestampMeta
    import doobie.implicits.javatime.JavaLocalTimeMeta
    import doobie.postgres.implicits._

    def getVideos(s: String): Query0[Video] =
      sql"""
        SELECT * FROM video
        WHERE title ILIKE ${s} OR orig_title LIKE ${s}
        LIMIT 50
         """.query[Video]

    def getCompilations(s: String): Query0[Compilation] =
      sql"""
        SELECT * FROM compilation
        WHERE title ILIKE ${s} OR orig_title LIKE ${s}
        LIMIT 50
         """.query[Compilation]

    def hasFreeAccess(c: Content): Query0[String] =
      sql"""
        SELECT vendor FROM item_free
        WHERE item_id = ${c.id}
         """.query[String]

    def getSubscriptions(c: Content): Query0[Subscription] =
      sql"""
        SELECT s.id, s.vendor, title, trial_price, trial_duration, price, duration, currency, s.created
        FROM item_subscription AS i
        INNER JOIN subscription AS s
            ON s.id = i.subscription_id
        WHERE item_id = ${c.id}
         """.query[Subscription]

    def getPurchases(c: Content): Query0[Purchase] =
      sql"""
        SELECT p.id, p.item_id, p.vendor, p.min_price, p.max_price, p.duration, p.currency, p.quality, p.created
        FROM item_purchase AS i
        INNER JOIN purchase AS p
            ON p.item_id = i.foreign_id
        WHERE i.item_id = ${c.id}
         """.query[Purchase]

    def getVideo(item: Video): Query0[Video] =
      sql"""
         SELECT * FROM video
         WHERE title = ${item.title} AND year = ${item.year}
         """.query[Video]

    def getSubscription(s: Subscription): Query0[Subscription] =
      sql"""
         SELECT * FROM subscription
         WHERE title = ${s.title} AND vendor = ${s.vendor}
         """.query[Subscription]

    def getCompilation(item: Compilation): Query0[Compilation] =
      sql"""
         SELECT * FROM compilation
         WHERE title = ${item.title} AND years @> ${item.years}
         """.query[Compilation]

    def createVideo(item: Video): Update0 =
      sql"""
         INSERT INTO video (id, title, orig_title, year)
            SELECT ${item.id}, ${item.title}, ${item.origTitle}, ${item.year}
         WHERE NOT EXISTS (
            SELECT 1 FROM video WHERE title = ${item.title} AND year = ${item.year}
          )
         """.update

    def createCompilation(item: Compilation): Update0 =
      sql"""
         INSERT INTO compilation (id, title, orig_title, years, seasons_count)
            SELECT ${item.id}, ${item.title}, ${item.origTitle}, ${item.years}, ${item.seasons}
         WHERE NOT EXISTS (
            SELECT 1 FROM compilation WHERE title = ${item.title}
          )
         """.update

    def createFreeAccess(f: ItemFreeAccess): Update0 =
      sql"""
         INSERT INTO item_free (id, item_id, vendor, created)
            SELECT ${f.id}, ${f.itemId}, ${f.vendor}, ${f.created}
         WHERE NOT EXISTS (
            SELECT 1 FROM item_free WHERE item_id = ${f.itemId} AND vendor = ${f.vendor}
         )
    """.update

    def createSubscription(s: Subscription): Update0 =
      sql"""
         INSERT INTO subscription (id, vendor, title, trial_price, trial_duration, price, duration, currency, created)
            SELECT ${s.id}, ${s.vendor}, ${s.title}, ${s.trialPrice}, ${s.trialDuration}, ${s.price}, ${s.duration}, ${s.currency}, ${s.created}
         WHERE NOT EXISTS (
            SELECT 1 FROM subscription WHERE title = ${s.title} AND vendor = ${s.vendor}
         )
         """.update

    def createItemSubscription(is: ItemSubscription): Update0 =
      sql"""
         INSERT INTO item_subscription (id, item_id, subscription_id, vendor, created)
            SELECT ${is.id}, ${is.itemId}, ${is.subId}, ${is.vendor}, ${is.created}
         WHERE NOT EXISTS (
            SELECT 1 FROM item_subscription WHERE item_id = ${is.itemId} AND subscription_id = ${is.subId} AND vendor = ${is.vendor}
         )
         """.update

    def createPurchase(p: Purchase): Update0 =
      sql"""
         INSERT INTO purchase (id, item_id, vendor, min_price, max_price, duration, currency, quality, created)
         VALUES (${p.id}, ${p.foreignId}, ${p.vendor}, ${p.minPrice}, ${p.maxPrice}, ${p.duration}, ${p.currency}, ${p.quality}, ${p.created})
         """.update

    def createPurchaseAccess(ip: ItemPurchase): Update0 =
      sql"""
         INSERT INTO item_purchase (id, item_id, foreign_id, vendor, created)
         VALUES (${ip.id}, ${ip.itemId}, ${ip.foreignId}, ${ip.vendor}, ${ip.created})
    """.update

    def deleteSubscriptions(t: Timestamp, vendor: String): Update0 =
      sql"""
         DELETE FROM item_subscription
         WHERE created < $t AND vendor = $vendor;

         DELETE FROM subscription
         WHERE created < $t AND vendor = $vendor;
         """.update

    def deletePurchases(t: Timestamp, vendor: String): Update0 =
      sql"""
         DELETE FROM item_purchase
         WHERE created < $t AND vendor = $vendor;

         DELETE FROM purchase
         WHERE created < $t AND vendor = $vendor;
         """.update

    def deleteFreeAccess(t: Timestamp, vendor: String): Update0 =
      sql"""
         DELETE FROM item_free
         WHERE created < $t AND vendor = $vendor
         """.update

  }

  def layer: ZLayer[Blocking with DatabaseConfig with Logging, Throwable, DBService] = {
    def initDb(cfg: DatabaseConfig.Config): RIO[Logging, Unit] =
      Task {
        Flyway
          .configure()
          .validateMigrationNaming(false)
          .createSchemas(true)
          .dataSource(cfg.url, cfg.user, cfg.password)
          .load()
          .migrate()
      }.catchAll { e =>
        log.error(s"Database initialisation failed => ${e.getMessage}")
      }.unit

    def mkTransactor(cfg: DatabaseConfig.Config): ZManaged[Blocking, Throwable, HikariTransactor[Task]] =
      ZIO.runtime[Blocking].toManaged_.flatMap { implicit rt =>
        for {
          transactEC <- Managed.succeed(
            rt.environment
              .get[Blocking.Service]
              .blockingExecutor
              .asEC
          )
          connectEC = rt.platform.executor.asEC
          transactor <- HikariTransactor
            .newHikariTransactor[Task](
              cfg.driver,
              cfg.url,
              cfg.user,
              cfg.password,
              connectEC,
              Blocker.liftExecutionContext(transactEC)
            )
            .toManaged
        } yield transactor
      }

    ZLayer.fromManaged {
      for {
        cfg        <- getDatabaseConfig.toManaged_
        _          <- initDb(cfg).toManaged_
        transactor <- mkTransactor(cfg)
      } yield new LiveRepository(transactor)
    }
  }
}
