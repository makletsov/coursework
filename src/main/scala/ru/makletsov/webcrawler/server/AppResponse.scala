package ru.makletsov.webcrawler.server

import java.sql.Timestamp

import io.circe.Decoder.Result
import io.circe._
import io.circe.generic.semiauto._
import ru.makletsov.webcrawler.database._

final case class AppResponse(
    content: Content,
    free: List[String],
    subscriptions: List[Subscription],
    purchases: List[Purchase]
)

object AppResponse {
  implicit val encoderResp: Encoder[AppResponse] = deriveEncoder
  implicit val decoderResp: Decoder[AppResponse] = deriveDecoder

  implicit val encoderCont: Encoder[Content] = deriveEncoder
  implicit val decoderCont: Decoder[Content] = deriveDecoder

  implicit val encoderVideo: Encoder[Video] = deriveEncoder
  implicit val decoderVideo: Decoder[Video] = deriveDecoder

  implicit val encoderCompilation: Encoder[Compilation] = deriveEncoder
  implicit val decoderCompilation: Decoder[Compilation] = deriveDecoder

  implicit val encoderSubscription: Encoder[Subscription] = deriveEncoder
  implicit val decoderSubscription: Decoder[Subscription] = deriveDecoder

  implicit val encoderPurchase: Encoder[Purchase] = deriveEncoder
  implicit val decoderPurchase: Decoder[Purchase] = deriveDecoder

  implicit val TimestampFormat : Encoder[Timestamp] with Decoder[Timestamp] = new Encoder[Timestamp] with Decoder[Timestamp] {
    override def apply(a: Timestamp): Json = Encoder.encodeLocalDateTime.apply(a.toLocalDateTime)

    override def apply(c: HCursor): Result[Timestamp] = Decoder.decodeLong.map(s => new Timestamp(s)).apply(c)
  }
}
