package ru.makletsov.webcrawler.server

import zio._
import zio.logging.log
import zio.interop.catz._
import io.circe._
import io.circe.literal.JsonStringContext
import org.http4s._
import org.http4s.circe._
import org.http4s.implicits._
import org.http4s.dsl.Http4sDsl
import org.http4s.server.Router
import org.http4s.server.middleware.CORS
import org.http4s.server.blaze.BlazeServerBuilder
import ru.makletsov.webcrawler.config.getAppConfig
import ru.makletsov.webcrawler.layers._
import ru.makletsov.webcrawler.database._
import ru.makletsov.webcrawler.database.Repository._

object HttpAppService {

  val run: RIO[AppEnv, ExitCode] =
    for {
      cfg <- getAppConfig
      _   <- log.info(s"Http server starting on ${cfg.http.baseUrl}")
      httpApp = Router[AppTask]("/" -> HttpAppService.routes).orNotFound

      _ <- ZIO.runtime[AppEnv] >>= { implicit rts =>
        BlazeServerBuilder
          .apply[AppTask](rts.platform.executor.asEC)
          .bindHttp(cfg.http.port, "0.0.0.0")
          .withHttpApp(CORS(httpApp))
          .serve
          .compile
          .drain
          .catchAll { e =>
            log.error(s"From HttpServer => ${e.getMessage}")
          }
      }
    } yield ExitCode.success

  private def routes: HttpRoutes[AppTask] = {

    val dsl: Http4sDsl[AppTask] = Http4sDsl[AppTask]

    import dsl._

    implicit def circeJsonDecoder[A: Decoder]: EntityDecoder[AppTask, A] = jsonOf[AppTask, A]

    implicit def circeJsonEncoder[A: Encoder]: EntityEncoder[AppTask, A] = jsonEncoderOf[AppTask, A]

    HttpRoutes.of[AppTask] {
      case GET -> Root => Ok("Welcome to the webcrawler service")

      case GET -> Root / queryString =>
        val query = queryString + "%"

        val maybeResp = for {
          videos       <- getVideos(query)
          compilations <- getCompilations(query)
          content = videos ++ compilations
          respItems <- ZIO.foreach(content)(getResponseItem)
          response <- respItems match {
            case List() => NoContent()
            case _      => Ok(respItems)
          }
        } yield response

        maybeResp.catchAll { e =>
          log.error(s"From router: ${e.getMessage}") *> ServiceUnavailable()
        }
    }
  }

  private def getResponseItem(c: Content) = {
    for {
      hasFreeAccess <- hasFreeAccess(c)
      subscriptions <- getSubscriptions(c)
      purchases     <- getPurchases(c)
    } yield AppResponse(c, hasFreeAccess, subscriptions, purchases)
  }
}
