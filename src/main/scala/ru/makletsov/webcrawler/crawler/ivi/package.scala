package ru.makletsov.webcrawler.crawler

import java.time._
import java.util.UUID
import java.sql.Timestamp

import zio._

import ru.makletsov.webcrawler.database._
import ru.makletsov.webcrawler.crawler.ivi.parser._

package object ivi {

  val vendor = "ivi"

  def generateUUID: UIO[UUID] = ZIO.effectTotal(UUID.randomUUID())
  def getTime: UIO[Timestamp]   = ZIO.effectTotal(Timestamp.from(Instant.now()))

  def convertToVideo(iv: IviVideo): UIO[Video] = {
    for {
      uuid <- generateUUID
      video = Video(uuid, iv.title, iv.orig_title, iv.year)
    } yield video
  }

  def convertToCompilation(iv: IviCompilation): UIO[Compilation] = {
    for {
      uuid <- generateUUID
      compilation = Compilation(uuid, iv.title, iv.orig_title, iv.years, iv.seasons_count)
    } yield compilation
  }

  def getFreeAccess(itemId: UUID): UIO[ItemFreeAccess] = {
    for {
      uuid   <- generateUUID
      time   <- getTime
      access = ItemFreeAccess(uuid, itemId, vendor, time)
    } yield access
  }

  def convertToSubscriptions(ss: Set[IviSubscription]): UIO[Set[Subscription]] = {
    ZIO.foreach(ss)(convertToSubscription)
  }

  def convertToSubscription(s: IviSubscription): UIO[Subscription] = {
    for {
      uuid   <- generateUUID
      time   <- getTime
      access = Subscription(uuid, vendor, s.product_title, s.price, s.renewal_initial_period, s.renewal_price, s.duration, s.currency, time)
    } yield access
  }

  def getSubscriptionAccess(itemId: UUID, subscriptionId: UUID): UIO[ItemSubscription] = {
    for {
      uuid   <- generateUUID
      time   <- getTime
      access = ItemSubscription(uuid, itemId, subscriptionId, vendor, time)
    } yield access
  }

  def convertToPurchase(p: IviPurchase): Task[Purchase] = {
    for {
      uuid   <- generateUUID
      time   <- getTime
      access = Purchase(uuid, p.object_id, vendor, p.price, p.price, p.duration.getOrElse(0), p.currency, p.quality, time)
    } yield access
  }

  def getPurchaseAccess(itemId: UUID, iviId: Int): Task[ItemPurchase] = {
    for {
      uuid   <- generateUUID
      time   <- getTime
      purchase = ItemPurchase(uuid, itemId, iviId, vendor, time)
    } yield purchase
  }
}
