package ru.makletsov.webcrawler.crawler.ivi.parser

sealed trait IviItem

case class IviCategory(
    id: Int,
    title: String,
    genres: Vector[IviGenre]
) extends IviItem

case class IviGenre(
    id: Int,
    category_id: Int,
    title: String,
    catalogue_count: Int
)

trait IviContent extends IviItem {
  val id: Int
  val content_paid_types: Seq[String]
}

case class IviVideo(
    id: Int,
    title: String,
    orig_title: String,
    year: Int,
    content_paid_types: Seq[String]
) extends IviContent

case class IviCompilation(
    id: Int,
    title: String,
    orig_title: String,
    years: Vector[Int],
    seasons_count: Int,
    content_paid_types: Seq[String]
) extends IviContent

case class IviSubscription(
    product_title: String,
    price: Double,
    renewal_initial_period: Int,
    renewal_price: Double,
    duration: Int,
    currency: String
) extends IviItem

case class IviPurchase(
    object_id: Int,
    price: Double,
    currency: String,
    quality: String,
    duration: Option[Int]
) extends IviItem

object ParsingModel {

  import io.circe._
  import io.circe.generic.semiauto._

  implicit val categoryDecoder: Decoder[IviCategory] = deriveDecoder

  implicit val genreDecoder: Decoder[IviGenre] = deriveDecoder

  implicit val videoItemDecoder: Decoder[IviVideo] = deriveDecoder

  implicit val compilationItemDecoder: Decoder[IviCompilation] = deriveDecoder

  implicit val subscriptionDecoder: Decoder[IviSubscription] = deriveDecoder

  implicit val purchaseDecoder: Decoder[IviPurchase] = deriveDecoder
}

class ParsingException(message: String) extends Exception(message)
