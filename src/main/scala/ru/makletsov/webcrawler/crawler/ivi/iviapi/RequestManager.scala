package ru.makletsov.webcrawler.crawler.ivi.iviapi

import io.lemonlabs.uri.Url
import ru.makletsov.webcrawler.crawler.ivi.parser._

trait RequestManager {

  def getEntryUrl: Url

  def getSubscriptionsRequests: Seq[Url]

  def getCatalogueRequest: Url

  def getContentRequests(cs: Seq[IviCategory]): Seq[Url]

  def getPurchaseRequests(c: Seq[IviContent]): Seq[Url]

}
