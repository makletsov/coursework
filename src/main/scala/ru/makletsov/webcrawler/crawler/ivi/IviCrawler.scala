package ru.makletsov.webcrawler.crawler.ivi

import java.sql.Timestamp
import java.time.LocalDateTime

import zio._
import zio.logging._
import io.lemonlabs.uri.Url
import ru.makletsov.webcrawler.crawler.ivi.parser.{IviContent, IviParser}
import ru.makletsov.webcrawler.crawler.ivi.iviapi.{ApiService, RequestManager}
import ru.makletsov.webcrawler.database.Repository._
import ru.makletsov.webcrawler.database._
import ru.makletsov.webcrawler.layers._

class IviCrawler(
    requestManager: RequestManager,
    processor: Processor,
    scrapeRetries: Int,
    apiService: ApiService,
    parser: IviParser
) {

  private def saveSubscriptions(urls: Seq[Url]): RIO[AppEnv, Seq[Subscription]] = {
    ZIO
      .foreach(urls) { url =>
        val scraped = for {
          _         <- log.info(s"Getting for the subscriptions list to the $url")
          json      <- apiService.getResponseBody(url).retryN(scrapeRetries)
          _         <- log.info(s"Response received, trying to parse...")
          items     <- parser.parseSubscriptions(url, json)
          _         <- log.info(s"${items.size} items successfully parsed")
          processed <- processor.processSubscriptions(items)
          _         <- log.info(s"${processed.size} items have been successfully saved to database")
        } yield processed

        scraped.catchAll { e =>
          log.warn(e.getMessage) *> ZIO.succeed(Seq.empty)
        }
      }
      .map(_.flatten)
  }

  private def saveAllContent(url: Url, ss: Seq[Subscription]): RIO[AppEnv, Seq[IviContent]] = {
    val scraped = for {
      _         <- log.info(s"Getting for the content categories list to the $url")
      json       <- apiService.getResponseBody(url).retryN(scrapeRetries)
      _         <- log.info(s"Response received, trying to parse...")
      categories <- parser.parseCategories(url, json)
      _         <- log.info(s"${categories.size} content categories items successfully parsed")
      urls = requestManager.getContentRequests(categories)
      content <- ZIO.foreachParN(5)(urls)(saveContentBunch(_, ss))
    } yield content.flatten

    scraped.catchAll { e =>
      log.warn(e.getMessage) *> ZIO.succeed(Seq.empty)
    }
  }

  private def saveContentBunch(url: Url, ss: Seq[Subscription]): RIO[AppEnv, Seq[IviContent]] = {
    val scraped = for {
      _         <- log.info(s"Getting for the content items info to the $url")
      json      <- apiService.getResponseBody(url).retryN(scrapeRetries)
      _         <- log.info(s"Response received, trying to parse...")
      content   <- parser.parseContentItems(url, json)
      _         <- log.info(s"${content.size} content items successfully parsed")
      processed <- processor.processContent(content, ss)
      _         <- log.info(s"${processed.size} items have been successfully saved to database or up to date")
    } yield content

    scraped.catchAll { e =>
      log.warn(e.getMessage) *> ZIO.succeed(Seq.empty)
    }
  }

  private def savePurchases(urls: Seq[Url]): RIO[AppEnv, Seq[Purchase]] = {
    ZIO
      .foreachPar(urls) { url =>
        val scraped = for {
          _         <- log.info(s"Getting for the purchase items info to the $url")
          json      <- apiService.getResponseBody(url).retryN(scrapeRetries)
          _         <- log.info(s"Response received, trying to parse...")
          purchases <- parser.parsePurchases(url, json)
          _         <- log.info(s"${purchases.size} purchase items successfully parsed")
          processed <- processor.processPurchases(purchases)
          _         <- log.info(s"${processed.size} items successfully saved to database")
        } yield processed

        scraped.catchAll { e =>
          log.warn(e.getMessage) *> ZIO.succeed(Seq.empty)
        }
      }
      .map(_.flatten)
  }

  private def needPurchase(items: Seq[IviContent]): Seq[IviContent] =
    items.filter { item =>
      item.content_paid_types.contains("EST") ||
      item.content_paid_types.contains("TVOD")
    }

  private def cleanupOutdatedRecords(since: Timestamp, vendor: String): RIO[DBService, Unit] = {
    for {
      _ <- deleteFreeAccess(since, vendor)
      _ <- deletePurchases(since, vendor)
      _ <- deleteSubscriptions(since, vendor)
    } yield ()
  }

  def run: RIO[AppEnv, Unit] = {
    val startTime = ZIO.effect(Timestamp.valueOf(LocalDateTime.now()))

    val unsafe = for {
      since <- startTime
      _     <- cleanupOutdatedRecords(since, vendor)

      subscriptionsRequest = requestManager.getSubscriptionsRequests
      subscriptions <- saveSubscriptions(subscriptionsRequest)

      catalogueRequest = requestManager.getCatalogueRequest
      catalogue <- saveAllContent(catalogueRequest, subscriptions)

      needPay         = needPurchase(catalogue)
      purchaseRequest = requestManager.getPurchaseRequests(needPay)
      _ <- savePurchases(purchaseRequest)
    } yield ()

    unsafe.catchAll { e =>
      log.error(s"From Ivi crawler => fails cause: ${e.getMessage}")
    }
  }
}
