package ru.makletsov.webcrawler.crawler.ivi.parser

import zio._
import zio.logging._
import io.circe._
import io.circe.optics.JsonPath._

import io.lemonlabs.uri.Url

import IviParserImpl._
import ParsingModel._

class IviParserImpl extends IviParser {
  override def parseCategories(url: Url, json: Json): RIO[Logging, Seq[IviCategory]] =
    parseResponse(url, json, parseCategory(url), root.result.arr.getOption)

  override def parseContentItems(url: Url, json: Json): RIO[Logging, Seq[IviContent]] =
    parseResponse(url, json, parseContentItem(url), root.result.arr.getOption)

  override def parseSubscriptions(url: Url, json: Json): RIO[Logging, Seq[IviSubscription]] =
    parseResponse(url, json, parseSubscriptionOption(url), root.result.purchase_options.arr.getOption)

  override def parsePurchases(url: Url, json: Json): RIO[Logging, Seq[IviPurchase]] =
    parseResponse(url, json, parsePurchaseOption(url), root.result.purchase_options.arr.getOption)
}

private object IviParserImpl {
  private def parseCategory(url: Url)(json: Json): IO[ParsingException, IviCategory] = {
    ZIO
      .fromOption(json.as[IviCategory].toOption)
      .orElseFail(makeException(url))
  }

  private def parseContentItem(url: Url)(json: Json): IO[ParsingException, IviContent] = {
    val item = root.object_type.string.getOption(json).flatMap {
      case "video" => json.as[IviVideo].toOption
      case _       => json.as[IviCompilation].toOption
    }

    ZIO
      .fromOption(item)
      .orElseFail(makeException(url))
  }

  private def parseSubscriptionOption(url: Url)(json: Json): IO[ParsingException, IviSubscription] =
    ZIO
      .fromOption(json.as[IviSubscription].toOption)
      .orElseFail(makeException(url))

  private def parsePurchaseOption(url: Url)(json: Json): IO[ParsingException, IviPurchase] =
    ZIO
      .fromOption(json.as[IviPurchase].toOption)
      .orElseFail(makeException(url))

  private def makeException(url: Url) =
    new ParsingException("Cannot parse part of the response for the request: " + url)

  private def makeJsonException(json: Json) =
    new ParsingException("Cannot parse: " + json)

  private def parseResponse[A <: IviItem](url: Url,
                                          json: Json,
                                          f1: Json => RIO[Logging, A],
                                          f2: Json => Option[Vector[Json]]): RIO[Logging, Vector[A]] = {

    def splitIntoParts(j: Json): RIO[Logging, Vector[Json]] = {
      ZIO.fromOption {
        f2(j)
      }.orElse {
        ZIO.succeed(Vector.empty[Json])
      }
    }

    val parsed = for {
      jsonVector <- splitIntoParts(json)
      items      <- ZIO.foreach(jsonVector)(f1)
    } yield items

    parsed.catchAll { e =>
      log.warn(e.getMessage) *>
        ZIO.succeed(Vector.empty)
    }
  }
}
