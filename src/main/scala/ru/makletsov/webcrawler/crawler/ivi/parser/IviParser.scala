package ru.makletsov.webcrawler.crawler.ivi.parser

import io.circe.Json
import io.lemonlabs.uri.Url
import zio.RIO
import zio.logging.Logging

trait IviParser {
  def parseCategories(url: Url, json: Json): RIO[Logging, Seq[IviCategory]]

  def parseContentItems(url: Url, json: Json): RIO[Logging, Seq[IviContent]]

  def parseSubscriptions(url: Url, json: Json): RIO[Logging, Seq[IviSubscription]]

  def parsePurchases(url: Url, json: Json): RIO[Logging, Seq[IviPurchase]]
}
