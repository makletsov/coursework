package ru.makletsov.webcrawler.crawler.ivi

import zio._

import ru.makletsov.webcrawler.database._
import ru.makletsov.webcrawler.crawler.ivi.parser._

trait Processor {

  def processSubscriptions(ss: Seq[IviSubscription]): RIO[DBService, Seq[Subscription]]

  def processPurchases(ps: Seq[IviPurchase]): RIO[DBService, Seq[Purchase]]

  def processContent(cs: Seq[IviContent], ss: Seq[Subscription]): RIO[DBService, Seq[Content]]

  def processCompilation(c: IviCompilation, ss: Seq[Subscription]): ZIO[DBService, Throwable, Compilation]

  def processVideo(v: IviVideo, ss: Seq[Subscription]): ZIO[DBService, Throwable, Video]

}
