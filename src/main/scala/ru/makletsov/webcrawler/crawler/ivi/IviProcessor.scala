package ru.makletsov.webcrawler.crawler.ivi

import java.util.UUID

import zio._
import ru.makletsov.webcrawler.database._
import ru.makletsov.webcrawler.crawler.ivi.parser._
import ru.makletsov.webcrawler.database.Repository._

class IviProcessor extends Processor {

  override def processSubscriptions(ss: Seq[IviSubscription]): RIO[DBService, Seq[Subscription]] = {
    def process(s: IviSubscription): RIO[DBService, Subscription] =
      convertToSubscription(s) >>= createSubscription

    ZIO.foreach(ss)(process)
  }

  override def processPurchases(ps: Seq[IviPurchase]): RIO[DBService, Seq[Purchase]] = {
    def process(p: IviPurchase): RIO[DBService, Purchase] =
      convertToPurchase(p) >>= createPurchase

    ZIO.foreach(ps)(process)
  }

  override def processContent(cs: Seq[IviContent], ss: Seq[Subscription]): RIO[DBService, Seq[Content]] =
    ZIO.foreach(cs) {
      case c: IviVideo       => processVideo(c, ss)
      case c: IviCompilation => processCompilation(c, ss)
    }

  override def processCompilation(c: IviCompilation,
                                  ss: Seq[Subscription]): ZIO[DBService, Throwable, Compilation] = {
    for {
      comp    <- convertToCompilation(c)
      created <- createCompilation(comp)
      _       <- makeAccessOptions(created.id, c, ss)
    } yield created
  }

  override def processVideo(v: IviVideo, ss: Seq[Subscription]): ZIO[DBService, Throwable, Video] = {
    for {
      video   <- convertToVideo(v)
      created <- createVideo(video)
      _       <- makeAccessOptions(created.id, v, ss)
    } yield created
  }

  private def makeAccessOptions(id: UUID,
                                c: IviContent,
                                ss: Seq[Subscription]): RIO[DBService, Unit] =
    for {
      _ <- makeSubscriptionAccess(id, ss).when(hasSubscriptionAccess(c))
      _ <- makeFreeAccess(id).when(hasFreeAccess(c))
      _ <- makePurchaseAccess(id, c.id).when(hasPurchaseAccess(c))
    } yield ()

  private def hasFreeAccess(c: IviContent): Boolean =
    c.content_paid_types.contains("AVOD") || c.content_paid_types.contains("FVOD")

  private def hasSubscriptionAccess(c: IviContent): Boolean =
    c.content_paid_types.contains("SVOD")

  private def hasPurchaseAccess(c: IviContent): Boolean =
    c.content_paid_types.contains("EST") || c.content_paid_types.contains("TVOD")

  private def makeFreeAccess(id: UUID): RIO[DBService, AccessOption] =
    getFreeAccess(id) >>= createFreeAccess

  private def makePurchaseAccess(id: UUID, iviId: Int): RIO[DBService, AccessOption] =
    getPurchaseAccess(id, iviId) >>= createPurchaseAccess

  private def makeSubscriptionAccess(id: UUID, ss: Seq[Subscription]): RIO[DBService, Seq[AccessOption]] = {
    ZIO.foreach(ss) { s =>
      getSubscriptionAccess(id, s.id) >>= (sa => createSubscriptionAccess(sa))
    }
  }
}
