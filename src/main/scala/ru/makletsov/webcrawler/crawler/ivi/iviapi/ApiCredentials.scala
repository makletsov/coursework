package ru.makletsov.webcrawler.crawler.ivi.iviapi

case class IviCredentials(session: String, sessionData: String)

case class IviCredentialsProductionException(message: String) extends Exception(message)



