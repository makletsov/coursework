package ru.makletsov.webcrawler.crawler.ivi.iviapi

import zio._
import zio.blocking._
import zio.logging._
import io.circe._
import io.circe.parser._
import io.lemonlabs.uri.Url
import io.lemonlabs.uri.typesafe.dsl.urlToUrlDsl

import ru.makletsov.webcrawler.client._
import IviApiService._

class IviApiService(client: WebClientProvider, entryUrl: Url) extends ApiService {

  private val urlString = entryUrl.toString()

  private def getSessionCredentials: RIO[Blocking with Logging, IviCredentials] = {
    for {
      s  <- client.getCookie(urlString, "sessivi")
      sd <- client.getCookie(urlString, "session_data")
    } yield IviCredentials(s, sd)
  }

  override def getResponseBody(url: Url): RIO[Blocking with Logging, Json] = {
    val unsafe = for {
      cr <- getSessionCredentials
      fullUrl = getUrlWithCredentials(url, cr)
      plain <- client.getResponseContentAsString(fullUrl.toString())
      maybeJson = parse(plain)
      json <- ZIO.fromEither(maybeJson)
    } yield json

    unsafe.catchAll{
      e => log.warn(s"Cannot get response from the $url cause $e") *>
        ZIO.succeed(Json.Null)
    }
  }
}

object IviApiService {
  def make(client: WebClientProvider, entryUrl: Url): RIO[Blocking, ApiService] =
    for {
      _ <- client.visitUrl(entryUrl.toString())
      service = new IviApiService(client, entryUrl)
    } yield service

  private def getUrlWithCredentials(url: Url, credentials: IviCredentials): Url =
    url ? ("session"  -> credentials.session) ?
      ("session_data" -> credentials.sessionData)
}
