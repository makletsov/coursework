package ru.makletsov.webcrawler.crawler.ivi

import java.util.concurrent.TimeUnit

import io.lemonlabs.uri.Url
import ru.makletsov.webcrawler.config._
import zio._
import zio.logging.log
import zio.duration.Duration
import ru.makletsov.webcrawler.layers._
import ru.makletsov.webcrawler.crawler._
import ru.makletsov.webcrawler.client._
import ru.makletsov.webcrawler.crawler.ivi.parser._
import ru.makletsov.webcrawler.crawler.ivi.iviapi._

private class IviCrawlingTask extends CrawlingTask {
  override def run(scrapeRetries: Int, scrapingInterval: Duration): RIO[AppEnv, Unit] = {
    for {
      client <- SyncWebClient.make

      cfg <- getAppConfig
      iviCfg         = cfg.ivi
      entry          = iviCfg.entry
      apiEntry       = iviCfg.workingUrl
      appVersion     = iviCfg.defAppVersion
      subscriptionId = iviCfg.defSubId
      appVersions    = iviCfg.appVersions

      manager = new IviRequestManager(entry, apiEntry, appVersion, subscriptionId, appVersions)
      apiService <- IviApiService.make(client, Url.parse(iviCfg.entry))

      parser    = new IviParserImpl
      processor = new IviProcessor
      crawler   = new IviCrawler(manager, processor, iviCfg.scrapingRetries, apiService, parser)

      _ <- runSafeWithRepeat(crawler, getScrapingInterval(iviCfg.scrapingInterval))
    } yield ()
  }

  private def getScrapingInterval(hours: Long): Duration =
    Duration.apply(hours, TimeUnit.HOURS)

  private def runSafeWithRepeat(crawler: IviCrawler, i: Duration) =
    crawler.run
      .repeat(Schedule.spaced(i))
      .catchAll { e =>
        log.error(s"From Ivi crawler => ${e.getMessage}")
      }
}

object IviCrawlingTask {
  def run(scrapeRetries: Int, scrapingInterval: Duration): RIO[AppEnv, Unit] = {
    val task = new IviCrawlingTask

    task.run(scrapeRetries, scrapingInterval)
  }
}
