package ru.makletsov.webcrawler.crawler.ivi.iviapi

import io.circe.Json
import io.lemonlabs.uri.Url
import zio.RIO
import zio.blocking.Blocking
import zio.logging.Logging

trait ApiService {
  def getResponseBody(url: Url): RIO[Blocking with Logging, Json]
}
