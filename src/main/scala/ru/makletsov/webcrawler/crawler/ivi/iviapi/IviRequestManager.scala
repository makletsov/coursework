package ru.makletsov.webcrawler.crawler.ivi.iviapi

import io.lemonlabs.uri.Url
import io.lemonlabs.uri.typesafe.dsl._
import ru.makletsov.webcrawler.crawler.ivi.parser._

class IviRequestManager(
    entry: String,
    apiEntry: String,
    defaultAppVersion: Int,
    defaultSubscriptionId: Int,
    appVersions: Seq[Int]
) extends RequestManager {

  private val apiEnterPoint = Url.parse(apiEntry)

  override def getEntryUrl: Url = Url.parse(entry)

  override def getSubscriptionsRequests: Seq[Url] = {
    def subscribeOptionsQueryString(id: Int, apiVersion: Int): Url =
      apiEnterPoint / "billing" / "v1" / "purchase" / "options" / "" ?
        ("id"                           -> id) ?
        ("with_long_subscriptions"      -> 1) ?
        ("with_affiliate_subscriptions" -> 1) ?
        ("with_bundle_subscriptions"    -> 1) ?
        ("app_version"                  -> apiVersion)

    appVersions.map {
      subscribeOptionsQueryString(defaultSubscriptionId, _)
    }
  }

  override def getCatalogueRequest: Url = {
    apiEnterPoint / "categories" / "v5" ?
      ("fields"      -> "id,title,genres,") ?
      ("app_version" -> defaultAppVersion)
  }

  override def getContentRequests(cs: Seq[IviCategory]): Seq[Url] = {
    for {
      category <- cs
      genre    <- category.genres
      request  <- getGenreRequest(genre, category)
    } yield request
  }

  private def getGenreRequest(genre: IviGenre, category: IviCategory): Seq[Url] = {
    val step       = 100
    val from       = 0 to genre.catalogue_count by step
    val to         = (step to genre.catalogue_count by step) :+ genre.catalogue_count
    val rangesList = from zip to

    for {
      (from, to) <- rangesList
    } yield
      apiEnterPoint / "catalogue" / "v5" ?
        ("category"    -> category.id) ?
        ("genre"       -> genre.id) ?
        ("from"        -> from) ?
        ("to"          -> to) ?
        ("fields"      -> "id,content_paid_types,object_type,title,orig_title,years,year,seasons_count,") ?
        ("app_version" -> defaultAppVersion)
  }

  override def getPurchaseRequests(c: Seq[IviContent]): Seq[Url] =
    c.map(getPurchaseOptionsRequest)

  private def getPurchaseOptionsRequest(content: IviContent): Url = {
    val pathPart = content match {
      case _: IviVideo => "content"
      case _           => "season"
    }

    apiEnterPoint / "billing" / "v1" / "purchase" / pathPart / "options" / "" ?
      ("id"          -> content.id) ?
      ("app_version" -> defaultAppVersion)
  }
}
