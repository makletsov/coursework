package ru.makletsov.webcrawler.crawler

import ru.makletsov.webcrawler.layers.AppEnv
import zio.RIO
import zio.duration.Duration

trait CrawlingTask {
  def run(scrapeRetries: Int, scrapingInterval: Duration): RIO[AppEnv, Unit]
}
