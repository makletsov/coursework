package ru.makletsov.webcrawler

import zio._
import zio.blocking.Blocking
import zio.logging.{LogFormat, LogLevel, Logging}

import zio.console._
import zio.clock._

import ru.makletsov.webcrawler.config._
import ru.makletsov.webcrawler.database._


object layers {

  type Layer0Env =
    AppConfig with Logging with Blocking with Clock

  type Layer1Env =
    Layer0Env with HttpConfig with DatabaseConfig

  type Layer2Env =
    Layer1Env with DBService

  type AppEnv = Layer2Env

  type AppTask[A] = RIO[AppEnv, A]

  object live {

    val loggingLayer: ZLayer[Any, Throwable, Console with Clock with Logging] =
      Console.live >+> Clock.live >+> Logging.console(
        logLevel = LogLevel.Trace,
        format = LogFormat.ColoredLogFormat()
      ) >+> Logging.withRootLoggerName("ivi-crawler")

    val layer0: ZLayer[Blocking, Throwable, Layer0Env] =
      Blocking.any ++ AppConfig.live ++ loggingLayer

    val layer1: ZLayer[Layer0Env, Throwable, Layer1Env] =
      HttpConfig.fromAppConfig ++ DatabaseConfig.fromAppConfig ++ IviConfig.fromAppConfig ++ ZLayer.identity

    val layer2: ZLayer[Layer1Env, Throwable, Layer2Env] =
      LiveRepository.layer ++ ZLayer.identity

    val appLayer: ZLayer[Blocking, Throwable, AppEnv] =
      layer0 >>> layer1 >>> layer2
  }
}
