package ru.makletsov.webcrawler

import zio._
import zio.duration.durationInt
import ru.makletsov.webcrawler.server._
import ru.makletsov.webcrawler.crawler.ivi._

object Main extends App {

  private val scrapeRetries = 1
  private val scrapingInterval = 1.day

  override def run(args: List[String]): ZIO[ZEnv, Nothing, ExitCode] = {
    HttpAppService.run
      .zipParLeft{
        IviCrawlingTask.run(scrapeRetries, scrapingInterval)
      }
      .provideSomeLayer[ZEnv]{
        layers.live.appLayer
      }
      .exitCode
  }
}
