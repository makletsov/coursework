package ru.makletsov.webcrawler.client

import ru.makletsov.webcrawler.crawler.ivi.iviapi.IviCredentialsProductionException
import zio._
import zio.blocking.Blocking

trait WebClientProvider {
  def visitUrl(url: String): RIO[Blocking, Unit]

  def getCookie(url: String, key: String): IO[IviCredentialsProductionException, String]

  def getResponseContentAsString(url: String): RIO[Blocking, String]
}
