package ru.makletsov.webcrawler.client

import zio._
import zio.blocking._
import java.net.URL

import scala.jdk.CollectionConverters._
import com.gargoylesoftware.htmlunit.{Page, WebClient}
import ru.makletsov.webcrawler.crawler.ivi.iviapi.IviCredentialsProductionException


class SyncWebClient private(private val sem: Semaphore, private val client: WebClient) extends WebClientProvider {

  override def visitUrl(url: String): RIO[Blocking, Unit] = {
    def visitPage: Page = client.getPage(url)

    sem.withPermit(effectBlocking(visitPage))
  }

  override def getCookie(url: String, key: String): ZIO[Any, IviCredentialsProductionException, String] = {
    val errorMessage = s"Failure to get credentials for the URL: $url"

    sem.withPermit {
      val option = client.getCookies(new URL(url))
        .asScala
        .filter(c => c.getName == key)
        .map(_.getValue)
        .headOption

      ZIO.fromOption(option)
        .orElseFail{
          IviCredentialsProductionException(errorMessage)
        }
    }
  }

  override def getResponseContentAsString(url: String): RIO[Blocking, String] =
    sem.withPermit {
      effectBlocking {
        val page: Page = client.getPage(url)

        page.getWebResponse.getContentAsString()
      }
    }
}

object SyncWebClient {
  def make: ZIO[Any, Throwable, SyncWebClient] = {
    for {
      s <- Semaphore.make(permits = 1)
      c = prepareClient(new WebClient())
      sc <- ZIO.effect(new SyncWebClient(s, c))
    } yield sc
  }

  private def prepareClient(client: WebClient) = {
    client.getOptions.setCssEnabled(false)
    client.getOptions.setJavaScriptEnabled(false)
    client.getOptions.setThrowExceptionOnScriptError(false)
    client.getOptions.setThrowExceptionOnFailingStatusCode(false)

    client
  }
}
