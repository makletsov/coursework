package ru.makletsov.webcrawler.config

import zio.ZLayer

import pureconfig.ConfigConvert
import pureconfig.generic.semiauto.deriveConvert

object IviConfig {

  final case class Config(
      entry: String,
      workingUrl: String,
      appVersions: Seq[Int],
      defAppVersion: Int,
      defSubId: Int,
      scrapingRetries: Int,
      scrapingInterval: Int
  )

  object Config {
    implicit val convert: ConfigConvert[Config] = deriveConvert
  }

  val fromAppConfig: ZLayer[AppConfig, Nothing, IviConfig] =
    ZLayer.fromService(_.ivi)

}