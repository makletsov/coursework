package ru.makletsov.webcrawler

import zio._

package object config {
  type AppConfig      = Has[AppConfig.Config]
  type HttpConfig     = Has[HttpConfig.Config]
  type DatabaseConfig = Has[DatabaseConfig.Config]
  type IviConfig      = Has[IviConfig.Config]

  val getAppConfig: URIO[AppConfig, AppConfig.Config] =
    ZIO.service

  val getHttpConfig: URIO[HttpConfig, HttpConfig.Config] =
    ZIO.service

  val getDatabaseConfig: URIO[DatabaseConfig, DatabaseConfig.Config] =
    ZIO.service

  val getIviConfig: URIO[IviConfig, IviConfig.Config] =
    ZIO.service
}
